<?php

namespace FifteenGroup\NovaReactiveField;

use FifteenGroup\NovaReactiveField\Traits\ReactiveField;
use Illuminate\Support\Arr;
use Laravel\Nova\Fields\Select;

class ReactiveSelect extends Select
{
    use ReactiveField;

    public $component = 'reactive-select-field';

    /**
     * Set each option's value as it's key
     *
     * @param $options
     * @return ReactiveSelect
     */
    public function optionsCombined($options)
    {
        $options = array_combine($options, $options);
        return parent::options($options);
    }
}
