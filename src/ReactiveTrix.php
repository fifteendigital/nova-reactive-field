<?php

namespace FifteenGroup\NovaReactiveField;

use FifteenGroup\NovaReactiveField\Traits\ReactiveField;
use Laravel\Nova\Fields\Trix;

class ReactiveTrix extends Trix
{
    use ReactiveField;

    public $component = 'reactive-trix-field';
}
