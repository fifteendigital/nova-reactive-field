export default {
    data: () => ({
        fieldsWithValues: {},
        ignoreOnLoadEmit: true
    }),

    created() {
        if (this.field.listensTo) {
            Nova.$on(this.field.listensTo, this.calculateValue);
        }
    },

    destroyed() {
        if (this.field.listensTo) {
            Nova.$off(this.field.listensTo, this.calculateValue);
        }
    },

    methods: {
        // name of value to broadcast after a change
        watches(value) {
            this.$watch(value, _.debounce(function () {
                this.broadcast(this.$data[value]);
            }, 500));

            let emitOnLoad = this.field.emitOnLoad && this.$data[value];

            emitOnLoad && this.broadcast(this.$data[value], true);
        },

        // broadcast value to listening components
        broadcast(value, emittedOnLoad = false) {
            if (this.field.broadcastFrom) {
                Nova.$emit(this.field.broadcastFrom, {
                    'field_name': this.field.attribute,
                    'value': value,
                    'emittedOnLoad': emittedOnLoad
                });
            }
        },

        // override this method to handle broadcast responses
        setValue(response) {
        },

        calculateValue: function (e) {
            this.fieldsWithValues.resourceId = this.resourceId;
            this.fieldsWithValues[e.field_name] = e.value;

            let ignoreOnLoadEmit = e.emittedOnLoad
                && this.ignoreOnLoadEmit
                && this.field.value;

            if (ignoreOnLoadEmit) {
                return;
            }

            let urlPrefix = '/fifteen-group/nova-compact-reactive-ui/calculate';

            Nova.request().post(
                `${urlPrefix}/${this.resourceName}/${this.field.attribute}`,
                this.fieldsWithValues
            ).then(response => {
                this.setValue(response, e.emittedOnLoad);
            });
        }
    }
}
